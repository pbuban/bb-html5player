window.onload = function() {
	// Video
	var video = document.getElementById("bb-video");
	// Buttons
	var playButton = document.getElementById("bb-play-pause");
	var muteButton = document.getElementById("bb-mute");
	// Sliders
	var volumeBar = document.getElementById("bb-volume-bar");

	// Event listener for the play/pause button
	playButton.addEventListener("click", function() {
		if (video.paused == true) {
			// Play the video
			video.play();

			// Update the button text to 'Pause'
			playButton.innerHTML = '<img src="images/player-pause.svg" />';
		} else {
			// Pause the video
			video.pause();

			// Update the button text to 'Play'
			playButton.innerHTML = '<img src="images/player-play.svg" />';
		}
	});

	// Event listener for the mute button
	muteButton.addEventListener("click", function() {
		if (video.muted == false) {
			// Mute the video
			video.muted = true;
			// Update the button text
			muteButton.innerHTML = '<img src="images/player-sound-off.svg" />';
		} else {
			// Unmute the video
			video.muted = false;
			// Update the button text
			muteButton.innerHTML = '<img src="images/player-sound-on.svg" />';
		}
	});




	//scrubber
	var $video = $("#bb-video");
	var $scrubber = $("#bb-scrubber");
	var $scrubHead = $("#bb-scrubber-head");
	var $scrubClick = $("#bb-scrub-click");
	var $progress = $("#bb-progress");

	$video.bind("timeupdate", videoTimeUpdateHandler);
	$scrubber.bind("mousedown", scrubberMouseDownHandler);
	//$scrubClick.bind("mousedown", scrubberMouseDownHandler);

	// Play the video when the seek handle is dropped
/*	$scrubHead.draggable({ axis: "x",
		stop: function(event, ui) {
	        if(ui.position.left>0) {
		        //alert('Return back');
		        $scrubHead.animate({"left": "0px"}, 600);
	        } else if(ui.position.left<-6800){
	            $scrubHead.animate({"left": "-6400px"}, 600);
	        }
    	}
    });*/
	function videoTimeUpdateHandler(e) {
	    var video = $video.get(0);
	    var percent = video.currentTime / video.duration;
	    updateProgressWidth(percent);
	}
	function scrubberMouseDownHandler(e) {
	    var $this = $(this);
	    var x = e.pageX - $this.offset().left;
	    var percent = x / $this.width();
	    updateProgressWidth(percent);
	    updateVideoTime(percent);
	}
	function updateProgressWidth(percent) {
	    $progress.width((percent * 100) + "%");
	    //$scrubHead.
	}
	function updateVideoTime(percent) {
	    var video = $video.get(0);
	    video.currentTime = percent * video.duration;
	    console.log(percent + " : " +video.currentTime);
	}

	new Dragdealer('bb-scrubber', {
		animationCallback: function(x, y){
			//console.log();
			updateVideoTime(Math.round(x * 100));
		}
	});
}